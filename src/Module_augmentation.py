import Augmentor
import glob, os, shutil

ipath = '/home/input' # direction input != direction output
opath = '/home/output' # direction output != direction input

nums = 100

def n1(a,b):
	p1 = Augmentor.Pipeline(a,b)
	p1.skew_top_bottom(probability=1, magnitude=0.05)
	p1.sample(nums)

def n2(a,b):
	p2 = Augmentor.Pipeline(a,b)
	p2.skew_corner(probability=1, magnitude= 0.03)
	p2.sample(nums)

def n3(a,b):
	p3 = Augmentor.Pipeline(a,b)
	p3.skew_left_right(probability=1, magnitude=0.1)
	p3.sample(nums)

def n4(a,b):
	p4 = Augmentor.Pipeline(a,b)
	p4.rotate_without_crop(probability=1, max_left_rotation=6, max_right_rotation=6, \
		expand=False, fillcolor=None)
	p4.sample(nums)

def n5(a,b):
	p5 = Augmentor.Pipeline(a,b)
	p5.random_erasing(probability=1, rectangle_area=0.4)
	p5.sample(nums)

def n6(a,b):
	p6 = Augmentor.Pipeline(a,b)
	p6.random_brightness(probability=1, min_factor=0.7, max_factor=1.3)
	p6.rotate_without_crop(probability=.4, max_left_rotation=6, max_right_rotation=6, \
		expand=False, fillcolor=None)
	p6.skew_top_bottom(probability=0.4, magnitude=0.05)
	p6.sample(nums)  # độ sáng : tăng các màu -> trắng, giảm các màu -> đen

def n7(a,b):
	p7 = Augmentor.Pipeline(a,b)
	p7.random_contrast(probability=1, min_factor=0.9, max_factor=1.3)
	p7.skew_left_right(probability=0.4, magnitude=0.1)
	p7.skew_top_bottom(probability=0.4, magnitude=0.05)
	p7.rotate_without_crop(probability=0.4, max_left_rotation=6, max_right_rotation=6, \
		expand=False, fillcolor=None)
	p7.sample(nums)

def n8(a,b):
	p8 = Augmentor.Pipeline(a,b)
	p8.random_distortion(probability=1, grid_width=2, grid_height=2, magnitude=2)
	p8.sample(nums)

def n9(a,b):
	p9 = Augmentor.Pipeline(a,b)
	p9.random_color(probability=1, min_factor=1.2, max_factor=1.4)
	p9.skew_left_right(probability=0.4, magnitude=0.1)
	p9.skew_top_bottom(probability=0.4, magnitude=0.05)
	p9.sample(nums)

def n10(a,b):
	p = Augmentor.Pipeline(a,b)
	p.flip_left_right(probability=1)
	p.sample(nums)

def rename(dir, pattern, titlePattern):
    x = 1
    for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
        title, ext = os.path.splitext(os.path.basename(pathAndFilename))
        os.rename(pathAndFilename, os.path.join(dir, titlePattern % x + ext))
        x+=1

def main():
	print(f"Doing")
	outpath1 = opath +'/skewtb'
	outpath2 = opath +'/skewcorner'
	outpath3 = opath +'/skewlr'
	outpath4 = opath +'/rotate_ncrop'
	outpath5 = opath +'/rd_erasing'
	outpath6 = opath +'/rd_bright'
	outpath7 = opath +'/rd_contrast'
	outpath8 = opath +'/rd_distort'
	outpath9 = opath +'/rd_color'
	dest = opath +'/Auged'
	paths = [outpath1,outpath2,outpath3,outpath4,outpath5,outpath6,outpath7,outpath8,outpath9]
	n1(ipath,outpath1)
	n2(ipath,outpath2)
	n3(ipath,outpath3)
	n4(ipath,outpath4)
	n5(ipath,outpath5)
	n6(ipath,outpath6)
	n7(ipath,outpath7)
	n8(ipath,outpath8)
	n9(ipath,outpath9)
	for source in paths:
		files = glob.iglob(os.path.join(source, "*.jpg"))
		for file in files:
			shutil.move(file, opath)
		os.rmdir(source)
	n10(opath,dest)
	files2 = glob.iglob(os.path.join(dest, "*.jpg"))
	for file2 in files2:
		shutil.move(file2, opath)
	rename(opath, r'*.jpg', r's%s')
	os.rmdir(dest)

try:
	os.makedirs(opath)
except:
	pass
main()
